/* create a class Bottle  with non-static variables
 brand 
 quantity
 price
create constructor with 3arguments
create method view()
note:get data from user dynamically*/

import java.util.Scanner;
class Bottle
{
String brand;
int quantity;
float price;
public Bottle(String brand, int quantity, float price)
{
this.brand= brand;
this.quantity= quantity;
this.price= price;
}
public void view()
{
System.out.println("brand is"+ brand);
System.out.println("quantity is"+ quantity);
System.out.println("price is"+ price);
}
public static void main (String[] args)
{
Scanner ob=new Scanner(System.in);
System.out.println("enter the brand");
String b= ob.next();
System.out.println("enter quantity");
int q= ob.nextInt();
System.out.println("enter price");
float p= ob.nextFloat();
Bottle B1= new Bottle(b,q, p);
B1.view( );
}
}

